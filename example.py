import os

current_dir = os.getcwd()
test_dir = os.path.join(current_dir, "test")


###-----------------------------------------------------
print("\n\n---------Cpconfig---------")
from cpconfig import Cpconfig
cpconfig = Cpconfig()

# Метод для получения информации о лицензии КриптоПро CSP
print(cpconfig.get_license_info())

###-----------------------------------------------------
print("\n\n---------Csptest---------")
from csptest import Csptest
csptest = Csptest()

# Метод для получения информации о КриптоПро CSP
print(csptest.get_cryptopro_info())

###-----------------------------------------------------
print("\n\n---------Cryptcp---------")
from cryptcp import Cryptcp
cryptcp = Cryptcp()

# Проверка прикрепленной подписи
doc_sign_dir = os.path.join(test_dir,"file+sign","Анатомия.pdf.sig")
out_dir = os.path.join(test_dir,"out")
print(cryptcp.verify(doc_sign_dir,out=out_dir, print_cosole=True))

# Проверка открепленной подписи
doc_sign_dir = os.path.join(test_dir,"file_sign","Анатомия.pdf")
sign_dir = os.path.join(test_dir,"file_sign","Анатомия.pdf.sig")
print(cryptcp.verify(doc_sign_dir,sign=sign_dir, print_cosole=True))

###-----------------------------------------------------
print("\n\n---------Certmgr---------")
from certmgr import Certmgr
certmgr = Certmgr()

# Установка сертификата
store = "uRoot"
ca_dir = os.path.join(test_dir,"cert","ca.cer")
certmgr.inst(store, ca_dir, auto_answer="o")

# Установка сертификата
store = "uRoot"
ca_dir = os.path.join(test_dir,"cert","ca.cer")
print(certmgr.inst(store, ca_dir, auto_answer="o"))

# Найти установленный сертификат
print(certmgr.find_ca_for_keyid(keyid="39b9275375aa9bd0a63bdb843392af7115bad020",store=store))

# Удалить сертификат
print(certmgr.delete(keyid="39b9275375aa9bd0a63bdb843392af7115bad020", store=store))

# Получить обьект сертификата из файла сертификата
ca_dir = os.path.join(test_dir,"cert","ca.cer")
print(certmgr.get_certs_info(ca_dir))

# Получить обьект сертификата из файла сертификата CRL
crl_dir = os.path.join(test_dir,"cert","crl.crl")
print(certmgr.get_certs_info(crl_dir,crl=True))

# Получить обьект сертификата из подписи
doc_sign_dir = os.path.join(test_dir,"file+sign","Анатомия.pdf.sig")
print(certmgr.get_certs_info(doc_sign_dir))