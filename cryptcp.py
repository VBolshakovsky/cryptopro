#!/usr/bin/env python3
# coding: utf-8
"""
Description: Working with cryptcp
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 31.08.2023
Links:
https://www.cryptopro.ru/products/other/cryptcp
https://www.cryptopro.ru/sites/default/files/products/cryptcp/cryptcp_5.0.x.pdf
Comment:
- cryptcp.exe в Windows, не поставляется вместе КриптоПро, его нужно скачать 
отдельно и скопировать в папку C:\\Program Files (x86)\\Crypto Pro\\CSP
- инструкция устаревшая и не отражает всего, лучше пользоваться -help консоли
"""
import platform
import os
import re
from typing import Optional
from сommand import Command


class CryptcpError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class Cryptcp(Command):
    """
    ### Класс для работы с утилитой cryptcp, входящей в состав КриптоПро
    
    Args:
        - path : путь к исполняемому файлу cryptcp

    Returns:
    (заполняется после вызова метода)
        - command   : команда
        - response  : ответ
        - error     : ошибка
    """

    def __init__(self, path: Optional[str] = None):

        super(Cryptcp, self).__init__()

        self.path = path

        self._get_path_cryptcp()

    def _get_path_cryptcp(self) -> str:
        """
        ### Возвращает путь к исполняемому файлу cryptcp.

        Этот метод проверяет, имеет ли атрибут path значение None, 
        и присваивает соответствующее значение в зависимости от текущей операционной системы.
        Если атрибут «путь» уже установлен, он возвращает существующее значение

        Returns:
            - str: путь к исполняемому файлу cryptcp

        Errors:
            - CryptcpError: Если атрибут «путь» не является допустимым путем к файлу
        """

        if self.path is None:
            if platform.system() == 'Linux':
                self.path = '/opt/cprocsp/bin/amd64/cryptcp'
            elif platform.system() == 'Windows':
                self.path = 'C:\\Program Files (x86)\\Crypto Pro\\CSP\\cryptcp.exe'
                self.shell = False
                self.encoding = '866'

        self._check_path(self.path)

        return self.path

    def _check_path(self, *args) -> None:
        """
        ### Проверка, существуют ли данные пути

        Args:
            - *args: пути, которые необходимо проверить

        Errors:
            CryptcpError: Если какой-либо из путей не существует.
        """

        for arg in args:
            if arg is not None and not os.path.exists(arg):
                self.error = f"Директория или файл отсутсвует: {arg}"
                raise CryptcpError(f"Директория или файл отсутсвует: {arg}")

    def _check_response_error(self) -> str:
        """
        ### Cодержит ли ответ ошибку, и верните соответствующее сообщение об ошибке
        
        Returns:
            - str: сообщение об ошибке, если в ответе не обнаружена ошибка, пустая строка
        """

        if '[ErrorCode: 0x00000000]' in self.response:
            return ""

        else:
            match = re.search(r'ErrorCode: (.+)]', self.response)
            if match:
                error_code = match.group(1).lower()

                return self._get_error(error_code)
            else:
                return "Не удалось найти в выводе, строку формата ErrorCode:"

    def verify(self,
               file: str,
               *args,
               sign: Optional[str] = None,
               out: Optional[str] = None,
               work_dir: Optional[str] = None,
               dn: Optional[str] = None,
               thumbprint: Optional[str] = None,
               errchain: bool = True,
               nochain: bool = False,
               norev: bool = False,
               print_cosole: bool = False) -> bool:
        """
        ### Проверка одиночной подписи

        Args:
            - file          : Путь к проверяемому файлу
            - args          : Дополнительные аргументы, передаваемые команде проверки
            - sign          : Путь к файлу подписи
            - out           : Путь к выходному файлу + название самого файла
            - work_dir      : Рабочий каталог, который будет использоваться
            - dn            : Имя сертификата
            - thumbprint    : Отпечаток сертификата
            - errchain      : Выводить ли ошибку, если хотя бы один сертификат не прошел проверку
            - nochain       : Исключать ли проверку цепочки сертификатов
            - norev         : Исключить ли проверку отзыва
            - print_cosole  : Включить ли ответ
            
        Returns:
            - True, если проверка прошла успешно, в противном случае — False.
        
        Дополнительные аргументы:
            - verall       : проверять все подписи (иначе - только подписи авторов из КПС)
            - start        : открыть (запустить) полученный файл
            - xlongtype1   : проверить подпись CAdES-X Long Type 1, КПС будет проигнорирован
            - cadest       : проверить подпись CAdES-T
            - cadesbes     : проверить подпись CAdES-BES
            - nocades      : запретить использование вложенных в подпись доказательств
            - keepbadfiles : не удалять выходной файл при ошибке
            - fext <расш>  : задать расширение сообщения в папке, по умолчанию .sgn
            - issuer       : использовать RDN издателя для поиска
            - m            : осуществить поиск в хранилищах компьютера (LOCAL_MACHINE)
            - u            : осуществить поиск в хранилищах пользователя (CURRENT_USER)
            - <имя>        : название хранилища (по умолчанию "My")
            - f <файл>     : использовать сообщение или файл сертификата
            - all          : использовать все найденные сертификаты (по умолчанию для КПС)
            - q<N>         : если найдено менее N сертификатов, вывести запрос выбора нужного
            - nonet        : использовать только кэшированные URL при построении цепочки
    	"""

        if work_dir:
            file = os.path.join(work_dir, file)
            if sign:
                sign = os.path.join(work_dir, sign)
            if out:
                out = os.path.join(work_dir, out)
                filename = os.path.basename(file)
                name, ext = os.path.splitext(filename)
                out = os.path.join(out, name)

        self._check_path(file, sign)

        cmd = [f'{self.path} -verify']

        if file == sign or sign is None:
            cmd.append(f'-attached -f "{file}" "{file}"')
            if out:
                cmd.append(f'"{out}"')
        else:
            cmd.append(f'-detached "{file}" "{sign}" -f "{sign}"')
        if thumbprint:
            cmd.append(f'-thumbprint {thumbprint}')
        if dn:
            cmd.append(f'-dn {dn}')
        if errchain:
            cmd.append('-errchain')
        if nochain:
            cmd.append('-nochain')
        if norev:
            cmd.append('-norev')

        command = ' '.join(cmd)

        self.run_command(command, *args)
        self.error = self._check_response_error()

        if print_cosole:
            self._print_response()

        if self.error == "":
            return True

        return False

    def _get_error(self, error_code: str) -> str:
        """
        ### Возвращает сообщение об ошибке, соответствующее коду ошибки

        Args:
            error_code (str): код ошибки, для которого нужно получить сообщение об ошибке

        Returns:
            str: сообщение об ошибке, соответствующее коду ошибки
        """

        error = {
            '0X20000064': 'Мало памяти',
            '0X20000065': 'Не удалось открыть файл',
            '0X20000066': 'Операция отменена пользователем',
            '0X20000067': 'Некорректное преобразование BASE64',
            '0X20000068':
            'Если указан параметр \'-help\', то других быть не должно',
            '0X20000069': 'Файл слишком большой',
            '0X20000070': 'Произошла внутренняя ошибка',
            '0X200000C8': 'Указан лишний файл',
            '0X200000C9': 'Указан неизвестный ключ',
            '0X200000CA': 'Указана лишняя команда',
            '0X200000CB': 'Для ключа не указан параметр',
            '0X200000CC': 'Не указана команда',
            '0X200000CD': 'Не указан необходимый ключ:',
            '0X200000CE': 'Указан неверный ключ:',
            '0X200000CF':
            'Параметром ключа \'-q\' должно быть натуральное число',
            '0X200000D0': 'Не указан входной файл',
            '0X200000D1': 'Не указан выходной файл',
            '0X200000D2': 'Команда не использует параметр с именем файла',
            '0X200000D3': 'Не указан файл сообщения',
            '0X2000012C': 'Не удалось открыть хранилище сертификатов:',
            '0X2000012D': 'Сертификаты не найдены',
            '0X2000012E': 'Найдено более одного сертификата (ключ \'-1\')',
            '0X2000012F':
            'Команда подразумевает использование только одного сертификата',
            '0X20000130': 'Неверно указан номер',
            '0X20000131': 'Нет используемых сертификатов',
            '0X20000132':
            'Данный сертификат не может применяться для этой операции',
            '0X20000133': 'Цепочка сертификатов не проверена',
            '0X20000134':
            'Криптопровайдер, поддерживающий необходимый алгоритм не найден',
            '0X20000135': 'Ошибка при вводе пароля на контейнер',
            '0X20000136': 'Не удалось получить закрытый ключ сертификата',
            '0X20000190': 'Не указана маска файлов',
            '0X20000191': 'Указаны несколько масок файлов.',
            '0X20000192': 'Файлы не найдены',
            '0X20000193': 'Задана неверная маска',
            '0X20000194': 'Неверный хэш',
            '0X200001F4': 'Ключ \'-start\' указан, а выходной файл нет',
            '0X200001F5': 'Содержимое файла - не подписанное сообщение',
            '0X200001F6': 'Неизвестный алгоритм подписи',
            '0X200001F7': 'Сертификат автора подписи не найден',
            '0X200001F8': 'Подпись не найдена',
            '0X200001F9': 'Подпись не верна',
            '0X200001FA': 'Штамп времени не верен',
            '0X20000258': 'Содержимое файла - не зашифрованное сообщение',
            '0X20000259': 'Неизвестный алгоритм шифрования',
            '0X2000025A':
            'Не найден сертификат с соответствующим секретным ключом',
            '0X200002BC': 'Не удалось инициализировать COM',
            '0X200002BD': 'Контейнеры не найдены',
            '0X200002BE': 'Не удалось получить ответ от сервераё',
            '0X200002BF': 'Сертификат не найден в ответе сервера',
            '0X200002C0': 'Файл не содержит идентификатор запроса:',
            '0X200002C1': 'Некорректный адрес ЦС',
            '0X200002C2': 'Получен неверный Cookie',
            '0X200002C3': 'ЦС отклонил запрос',
            '0X200002C4': 'Ошибка при инициализации CURL',
            '0X20000320':
            'Серийный номер содержит недопустимое количество символов',
            '0X20000321': 'Неверный код продукта',
            '0X20000322': 'Не удалось проверить серийный номер',
            '0X20000323': 'Не удалось сохранить серийный номер',
            '0X20000324': 'Не удалось загрузить серийный номер',
            '0X20000325': 'Лицензия просрочена',
            '0X200000d0': 'Не указан входной файл',
        }

        if error.get(error_code.upper()):
            self.error = f"Ошибка выполнения cryptcp:{error_code}-{error.get(error_code.upper())}"
        else:
            self.error = f"Ошибка выполнения cryptcp:{error_code}-Не известный код ошибки"

        return self.error