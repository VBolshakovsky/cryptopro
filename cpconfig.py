#!/usr/bin/env python3
# coding: utf-8
"""
Description: Working with cpconfig
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 06.11.2023
Links:
Comment:
"""

import platform
import os
import re
from сommand import Command
from typing import Optional


class CpconfigError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class Cpconfig(Command):
    """
    ### Класс для работы с утилитой cpconfig, входящей в состав КриптоПро
    
    Args:
        - path : путь к исполняемому файлу cpconfig

    Returns:
    (заполняется после вызова метода)
        - command   : команда
        - response  : ответ
        - error     : ошибка
    """

    def __init__(self, path: Optional[str] = None):

        super(Cpconfig, self).__init__()

        self.path = path

        self._get_path_cpconfig()

    def _get_path_cpconfig(self) -> str:
        """
        ### Возвращает путь к исполняемому файлу cpconfig.

        Этот метод проверяет, имеет ли атрибут path значение None, 
        и присваивает соответствующее значение в зависимости от текущей операционной системы.
        Если атрибут «путь» уже установлен, он возвращает существующее значение

        Returns:
            - str: путь к исполняемому файлу cpconfig

        Errors:
            - CpconfigError: Если атрибут «путь» не является допустимым путем к файлу
        """

        if self.path is None:
            if platform.system() == 'Linux':
                self.path = '/opt/cprocsp/sbin/amd64/cpconfig'
            elif platform.system() == 'Windows':
                self.path = 'C:\\Program Files (x86)\\Crypto Pro\\CSP\\cpconfig.exe'
                self.shell = False
                self.encoding = '866'

        self._check_path(self.path)

        return self.path

    def _check_path(self, *args) -> None:
        """
        ### Проверка, существуют ли данные пути

        Args:
            - *args: пути, которые необходимо проверить

        Errors:
            CpconfigError: Если какой-либо из путей не существует.
        """

        for arg in args:
            if arg is not None and not os.path.exists(arg):
                self.error = f"Директория или файл отсутсвует: {arg}"
                raise CpconfigError(f"Директория или файл отсутсвует: {arg}")

    def _check_response_error(self) -> str:
        """
        ### Cодержит ли ответ ошибку, и верните соответствующее сообщение об ошибке
        
        Returns:
            - str: сообщение об ошибке, если в ответе не обнаружена ошибка, пустая строка
        """

        if 'License validity' in self.response:
            return ""

        else:
            return "Не удалось найти в выводе, строку License validity"

    def _get_error(self, error_code: str) -> str:
        """
        ### Возвращает сообщение об ошибке, соответствующее коду ошибки

        Args:
            error_code (str): код ошибки, для которого нужно получить сообщение об ошибке

        Returns:
            str: сообщение об ошибке, соответствующее коду ошибки
        """

        error = {
            '0X20000064': 'Мало памяти',
        }

        if error.get(error_code.upper()):
            self.error = f"Ошибка выполнения cpconfig:{error_code}-{error.get(error_code.upper())}"
        else:
            self.error = f"Ошибка выполнения cpconfig:{error_code}-Не известный код ошибки"

        return self.error

    def get_license_info(self, *args, **kwargs) -> str:
        """
        ### Метод для получения информации о лицензии КриптоПро CSP
        
        Возврат:
        - Словарь
            result = {
                "serial": номер ключа,
                "expires": колличество оставшихся дней,
                "type": тип лицензии,
            }
        """

        command = f'{self.path} -license -view'
        self.run_command(command=command)
        
        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return self._parse_info(self.response)

        return {}

    @staticmethod
    def _parse_info(response) -> dict:
        """
        ### Преобразует строку в пару ключ:значение
        
        Возврат:
        - Словарь
            result = {
                "serial": номер ключа,
                "expires": колличество оставшихся дней,
                "type": тип лицензии,
            }
        """
        result = {
            "serial": None,
            "expires": None,
            "type": None,
        }
        
        response = response.replace(":\n",":")

        lines = response.split("\n")
        for line in lines:
            if line.strip() == "":
                continue
            
            colon_pos = line.find(":")
            if colon_pos == -1:
                continue
            
            key = line[:colon_pos].strip()
            value = line[colon_pos+1:].strip()
            
            if key == "License validity":
                result["serial"] = value
            
            if key == "Expires":
                result["expires"] = value
            
            if key == "License type":
                result["type"] = value
        
        return result
