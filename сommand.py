#!/usr/bin/env python3
# coding: utf-8
"""
Description: Working with shell command line
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 31.08.2023
Links:
https://docs-python.ru/standart-library/modul-subprocess-python/klass-popen-modulja-subprocess/
Comment:
"""

from subprocess import Popen, PIPE
from typing import Optional, Callable, Dict


class CommandError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class Command(object):
    """ 
    ### Класс для работы с командной строкой
    
    Args:
    - binary        : данные, из вывода/ввода, будут возвращены в виде строк, а не байты
    - encoding      : кодировка для чтения/записи данных, None-системная кодировка. 
                    Например 'utf-8' или 'cp1251'
    - shell         : будет ли команда выполняться с помощью оболочки операционной системы
    - cwd           : рабочая директория, None-текущая
    - env           : словарь с переменными окружения
    - preexec_fn    : функция, которая будет запущена перед выполнением
    - errors        : функция, которая будет запущена при вознекновении ошибок
    - reply         : авто-ответ, строка когда консоль запрашивает подтверждение
    """

    def __init__(
        self,
        text: bool = True,
        encoding: Optional[str] = None,
        shell: bool = True,
        cwd: Optional[str] = None,
        env: Optional[dict] = None,
        preexec_fn: Optional[Callable] = None,
        errors: Optional[Callable] = None,
    ):

        self.text = text
        self.encoding = encoding
        self.shell = shell
        self.cwd = cwd
        self.env = env
        self.preexec_fn = preexec_fn
        self.errors = errors

        self.command = ""
        self.response = ""
        self.error = ""

    def get_props_dict(self) -> dict:
        """
        ### Возвращает словарь, содержащий свойства объекта Command
        """
        props = {
            'text': self.text,
            'encoding': self.encoding,
            'shell': self.shell,
            'cwd': self.cwd,
            'env': self.env,
            'preexec_fn': self.preexec_fn,
            'errors': self.errors
        }
        return props

    def run_command(self, command: str, *args, **kwargs):
        """
        ### Выполните команду с заданными аргументами

        Args:
            - command   : команда, которую необходимо выполнить
            - *args     : дополнительные позиционные аргументы
            - **kwargs  : дополнительные именованные аргументы

        Returns:
            Ответ от выполнения команды
        """
        auto_answer = None
        auto_answer = kwargs.pop("auto_answer", None)

        params = ' '.join(list(args))

        named_params = ' '.join(
            ['-%s %s' % (k, v) for k, v in kwargs.items() if v is not None])

        cmd = ' '.join([command, params, named_params])
        popen_params = self.get_props_dict()
        #print(f"Команда: {cmd}")

        self.command = cmd
        proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE, **popen_params)

        if auto_answer:
            proc.stdin.write(f"{auto_answer}\n")
        return self._response(*proc.communicate())

    def _response(self, stdout, stderr):
        """
        ### Выполняет команду и возвращает ответ

        Аргументы:
            - stdout  : стандартный вывод команды
            - stderr  : стандартный ошибки команды

        Returns:
            Ответ на выполнение команды

        Errors:
            CommandError: Если при выполнении команды произошла ошибка
        """
        if stderr:
            self.error = stderr
            #raise CommandError(stderr)
        self.response = stdout
        return stdout

    @classmethod
    def _dict_to_cmd_args(cls,
                          dictionary: Dict[str, str],
                          delimiter: str = "-") -> str:
        """
        ### Словарь в строку аргументов командной строки

        Args:
            - dictionary    : словарь для преобразования
            - delimiter     : разделитель, используемый между ключами аргументов и значениями.
                            По умолчанию «-».

        Returns:
            str: строка аргументов командной строки
        """
        args = []
        for key, value in dictionary.items():
            args.append(f"{delimiter}{key}")
            args.append(str(value))

        return " ".join(args)

    def _print_response(self) -> None:
        """
        ### Вывод ответа в консоль
        """
        if self.command != "":
            print(f"Команда:\n {self.command}\n")

        if self.response != "":
            print(f"Ответ:\n {self.response}\n")

        if self.error != "":
            print(f"Ошибка:\n {self.error}\n")