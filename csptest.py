#!/usr/bin/env python3
# coding: utf-8
"""
Description: Working with csptest
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 06.11.2023
Links:
Comment:
"""

import platform
import os
import re
from сommand import Command
from typing import Optional


class CsptestError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class Csptest(Command):
    """
    ### Класс для работы с утилитой csptest, входящей в состав КриптоПро
    
    Args:
        - path : путь к исполняемому файлу csptest

    Returns:
    (заполняется после вызова метода)
        - command   : команда
        - response  : ответ
        - error     : ошибка
    """

    def __init__(self, path: Optional[str] = None):

        super(Csptest, self).__init__()

        self.path = path

        self._get_path_csptest()

    def _get_path_csptest(self) -> str:
        """
        ### Возвращает путь к исполняемому файлу csptest.

        Этот метод проверяет, имеет ли атрибут path значение None, 
        и присваивает соответствующее значение в зависимости от текущей операционной системы.
        Если атрибут «путь» уже установлен, он возвращает существующее значение

        Returns:
            - str: путь к исполняемому файлу csptest

        Errors:
            - CsptestError: Если атрибут «путь» не является допустимым путем к файлу
        """

        if self.path is None:
            if platform.system() == 'Linux':
                self.path = '/opt/cprocsp/bin/amd64/csptest'
            elif platform.system() == 'Windows':
                self.path = 'C:\\Program Files (x86)\\Crypto Pro\\CSP\\csptest.exe'
                self.shell = False
                self.encoding = '866'

        self._check_path(self.path)

        return self.path

    def _check_path(self, *args) -> None:
        """
        ### Проверка, существуют ли данные пути

        Args:
            - *args: пути, которые необходимо проверить

        Errors:
            CsptestError: Если какой-либо из путей не существует.
        """

        for arg in args:
            if arg is not None and not os.path.exists(arg):
                self.error = f"Директория или файл отсутсвует: {arg}"
                raise CsptestError(f"Директория или файл отсутсвует: {arg}")

    def _check_response_error(self) -> str:
        """
        ### Cодержит ли ответ ошибку, и верните соответствующее сообщение об ошибке
        
        Returns:
            - str: сообщение об ошибке, если в ответе не обнаружена ошибка, пустая строка
        """

        if '[ErrorCode: 0x00000000]' in self.response:
            return ""

        else:
            match = re.search(r'ErrorCode: (.+)]', self.response)
            if match:
                error_code = match.group(1).lower()

                return self._get_error(error_code)
            else:
                return "Не удалось найти в выводе, строку формата ErrorCode:"

    def _get_error(self, error_code: str) -> str:
        """
        ### Возвращает сообщение об ошибке, соответствующее коду ошибки

        Args:
            error_code (str): код ошибки, для которого нужно получить сообщение об ошибке

        Returns:
            str: сообщение об ошибке, соответствующее коду ошибки
        """

        error = {
            '0X20000064': 'Мало памяти',
        }

        if error.get(error_code.upper()):
            self.error = f"Ошибка выполнения csptest:{error_code}-{error.get(error_code.upper())}"
        else:
            self.error = f"Ошибка выполнения csptest:{error_code}-Не известный код ошибки"

        return self.error

    def get_cryptopro_info(self, *args, **kwargs) -> dict:
        """
        ### Метод для получения информации о КриптоПро CSP
        
        Возврат:
        - Словарь
            result = {
                "CSP": версия,
                "OS": операционка,
                "CPU": процессор,
                "сryptopro": версия
            }
        """

        command = f'{self.path} -keyset -verifycontext'
        self.run_command(command=command)

        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return self._parse_info(self.response)

        return {}

    @staticmethod
    def _parse_info(response) -> dict:
        """
        ### Преобразует строку в пару ключ:значение
        
        Возврат:
        - Словарь
            result = {
                "CSP": версия,
                "OS": операционка,
                "CPU": процессор,
                "сryptopro": версия
            }
        """

        csp_match = re.search(r"CSP \(.+?\) (.*?) ", response)
        csp_text = csp_match.group(1)

        os_match = re.search(r"OS:(.*?) ", response)
        os_text = os_match.group(1)

        cpu_match = re.search(r"CPU:(.*?) ", response)
        cpu_text = cpu_match.group(1)

        crypto_pro_match = re.search(r"PP_NAME\): (.*?)\n", response)
        crypto_pro_text = crypto_pro_match.group(1)

        result = {
            "CSP": csp_text,
            "OS": os_text,
            "CPU": cpu_text,
            "сryptopro": crypto_pro_text
        }

        return result
