# Описание
Набор скриптов, для работы с консольными утилитами КриптоПро CSP

# Установка КриптоПро на linux
[Официальный мануал](https://support.cryptopro.ru/index.php?/Knowledgebase/Article/View/338/0/nstrojjk-rbochego-mest-pod-uprvleniem-os-linux-dlja-rboty-s-oblchnymi-sertifiktmi-ehlektronnojj-podpisi)

1. Загрузите архив ПО КриптоПро CSP для Linux (x64. deb) [по ссылке](https://www.cryptopro.ru/products/csp/downloads) (предварительно требуется регистрация).
- [linux-amd64_deb.tgz](https://cryptopro.ru/sites/default/files/private/csp/50/12922rc1/linux-amd64_deb.tgz)
2. Распакуйте загруженный архив: 

```
tar -xvf linux-amd64_deb.tgz && cd linux-amd64_deb
```

4. Установите основные пакеты КриптоПро CSP: 
```
sudo ./install.sh
sudo ./install_gui.sh
```

В результате в рабочей директории, буду сформированы следующие утилиты:
- cryptcp. Утилита командной строки для подписи и шифрования файлов.
- certmgr. Программа для работы с сертификатами, CRL и хранилищами.
- csptest. Программа для работы с контейнерами, получение информации о КриптоПро

4. Проверим
```
/opt/cprocsp/bin/amd64/cryptcp -help
/opt/cprocsp/bin/amd64/certmgr -help