#!/usr/bin/env python3
# coding: utf-8
"""
Description: Working with certmgr
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 06.11.2023
Links:
Comment:
"""

from datetime import datetime
import platform

import os
import re
from typing import Optional, List
from сommand import Command


class CertmgrError(Exception):
    """
    ### Пользовательский класс для работы с ошибками
    """


class PersonalInfo(object):

    def __init__(self, line):
        self.line = line
        self.cn = None
        self.inn = None
        self.email = None
        self._set_fields()

    def _set_fields(self):
        data = self._parse()

        self.cn = data.get('CN', None)
        self.email = data.get('e', None)

        if self.in_dict('ИНН', data):
            self.inn = data.get('ИНН', None)

        if self.in_dict('ИННЮЛ', data):
            self.inn = data.get('ИННЮЛ', None)

    def _parse(self):
        data = {}
        for item in self.line.split(', '):
            try:
                k, v = item.split('=')
                data[k] = v
            except:
                pass
        return data

    def in_dict(self, key, dict):
        return key in dict

    def as_string(self):
        return self.line

    def as_dict(self):
        return self._parse()

    def __repr__(self):
        return self.as_string()


class CertificateCRL(object):
    """
    Сертификат CRL
    """

    def __init__(self, issuer, valid_from, valid_to, id_key, count, crl_number,
                 data):
        self.issuer = issuer
        self.valid_from = valid_from
        self.valid_to = valid_to
        self.id_key = id_key
        self.count = count
        self.crl_number = crl_number
        self.data = data

    def __repr__(self):
        return "CRL:" + self.id_key

    def __str__(self):
        return "CRL:" + str(self.id_key)

    def as_dict(self):
        rez = self.__dict__
        rez["issuer"] = self.issuer.as_dict()
        rez["valid_from"] = self.valid_from.isoformat()
        rez["valid_to"] = self.valid_to.isoformat()
        rez.pop("data")

        return rez


class Certificate(object):
    """
    Сертификат
    """

    def __init__(
        self,
        issuer,
        subject,
        thumbprint,
        serial,
        id_key,
        id_key_ca,
        valid_from,
        valid_to,
        chain,
        data,
        ca: Optional[list] = None,
        crl: Optional[list] = None,
        ocsp: Optional[list] = None,
    ):
        self.issuer = issuer
        self.subject = subject
        self.thumbprint = thumbprint
        self.serial = serial
        self.id_key = id_key
        self.id_key_ca = id_key_ca
        self.valid_from = valid_from
        self.valid_to = valid_to
        self.chain = chain
        self.data = data
        self.ca = ca
        self.crl = crl
        self.ocsp = ocsp

    def __repr__(self):
        return self.serial

    def __str__(self):
        return self.serial

    def as_dict(self):
        rez = self.__dict__
        rez["issuer"] = self.issuer.as_dict()
        rez["subject"] = self.subject.as_dict()
        rez["valid_from"] = self.valid_from.isoformat()
        rez["valid_to"] = self.valid_to.isoformat()
        rez.pop("data")

        return rez


class Certmgr(Command):
    """
    ### Обертка над утилитой certmgr, входящей в состав Крипто-Про CSP
    """

    def __init__(self, path: Optional[str] = None):

        super(Certmgr, self).__init__()

        self.path = path

        self._get_path_certmgr()

    def _get_path_certmgr(self) -> str:
        """
        ### Возвращает путь к исполняемому файлу certmgr.

        Этот метод проверяет, имеет ли атрибут path значение None, 
        и присваивает соответствующее значение в зависимости от текущей операционной системы.
        Если атрибут «путь» уже установлен, он возвращает существующее значение

        Returns:
            - str: путь к исполняемому файлу certmgr

        Errors:
            - CertmgrError: Если атрибут «путь» не является допустимым путем к файлу
        """

        if self.path is None:
            if platform.system() == 'Linux':
                self.path = '/opt/cprocsp/bin/amd64/certmgr'
            elif platform.system() == 'Windows':
                self.path = 'C:\\Program Files (x86)\\Crypto Pro\\CSP\\certmgr.exe'
                self.shell = False
                self.encoding = '866'

        self._check_path(self.path)

        return self.path

    def _check_path(self, *args) -> None:
        """
        ### Проверка, существуют ли данные пути

        Args:
            - *args: пути, которые необходимо проверить

        Errors:
            CryptcpError: Если какой-либо из путей не существует.
        """

        for arg in args:
            if arg is not None and not os.path.exists(arg):
                self.error = f"Директория или файл отсутсвует: {arg}"
                raise CertmgrError(f"Директория или файл отсутсвует: {arg}")

    def _check_response_error(self) -> str:
        """
        ### Cодержит ли ответ ошибку, и верните соответствующее сообщение об ошибке
        
        Returns:
            - str: сообщение об ошибке, если в ответе не обнаружена ошибка, пустая строка
        """

        if '[ErrorCode: 0x00000000]' in self.response:
            return ""

        else:
            match = re.search(r'ErrorCode: (.+)]', self.response)
            if match:
                error_code = match.group(1).lower()

                return self._get_error(error_code)
            else:
                return "Не удалось найти в выводе, строку формата ErrorCode:"

    def _get_error(self, error_code: str) -> str:
        """
        ### Возвращает сообщение об ошибке, соответствующее коду ошибки

        Args:
            error_code (str): код ошибки, для которого нужно получить сообщение об ошибке

        Returns:
            str: сообщение об ошибке, соответствующее коду ошибки
        """

        error = {'0x20000064': 'Мало памяти'}

        if error.get(error_code):
            self.error = f"Ошибка выполнения certmgr:{error_code}-{error.get(error_code)}"
        else:
            self.error = f"Ошибка выполнения certmgr:{error_code}-Не известный код ошибки"

        return self.error

    def inst(self, store: str, file_path: str, *args,
             **kwargs) -> List[Certificate]:
        """
        ### Установка сертификатов

        Аргументы:
        - store u<имя>     Имя хранилища сертификатов пользователя (по умолчанию: uMy)
        - store m<имя>     Имя хранилища сертификатов компьютера
                          (вместе с -autodist можно указать только uMy или mMy)
            - My – для личных сертификатов
            - Root – для корневых сертификатов Удостоверяющих Центров
            - CA – для сертификатов промежуточных Удостоверяющих Центров
            и списков отозванных сертификатов
            - AddressBook – для сертификатов других пользователей
        - file <путь>      Путь к файлу с сертификатом или CRL
        - provname <имя>   Имя провайдера
        - provtype <тип>   Тип провайдера
        - container <имя>  Имя контейнера закрытого ключа
        - ask-container    Попросить пользователя выбрать из существующих контейнеров
        - to-container     При установке сертификата также положить его в контейнер
        - certificate*     Установить сертификат
        - crl              Установить CRL
        - pfx              Установить PFX
        - autodist         Автоматически распределить сертификаты из PFX по хранилищам
        - pin <пин-код>    Пин-код на контейнер закрытого ключа или пароль на PFX
        - newpin <пин-код> Пин-код на контейнер импортируемого из PFX ключа
        - carrier <путь>   Путь к носителю-приёмнику при импорте PFX
        - at_signature     Использовать ключ подписи вместо ключа обмена
        - all              Использовать все сертификаты/CRLs
        - silent           Использовать сертификат/CRL в неинтерактивном режиме
        - keep_exportable  Пометить импортированные ключи как экспортируемые
        - protected <вид>  Пометить импортированные ключи как protected: none/medium/high
        - trace <режим>    Уровень логирования для внутренних сообщений
        - tfmt <флаги>     Формат логирования для внутренних сообщений
        """

        cmd = [f'{self.path} -inst']
        cmd.append(f'-store "{store}"')
        cmd.append(f'-file "{file_path}"')
        command = ' '.join(cmd)

        self.run_command(command, *args, **kwargs)

        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return self._parse(self.response)

        return []

    def delete(self,
               store: str = 'uRoot',
               dn: Optional[str] = None,
               thumbprint: Optional[str] = None,
               keyid: Optional[str] = None,
               crl: bool = False,
               *args,
               **kwargs) -> bool:
        """
        ### Удаление сертификатов

        Аргументы:
        -store u<имя>     Имя хранилища сертификатов пользователя (по умолчанию: uMy)
        -store m<имя>     Имя хранилища сертификатов компьютера
            - My – для личных сертификатов
            - Root – для корневых сертификатов Удостоверяющих Центров
            - CA – для сертификатов промежуточных Удостоверяющих Центров
            и списков отозванных сертификатов
            - AddressBook – для сертификатов других пользователей
        -dn <CN=..O=..>   DName сертификата для фильтрации
        -thumbprint <хеш> Отпечаток сертификата для фильтрации
        -keyid <id>       ID ключа сертификата для фильтрации
        -authkeyid <id>   ID ключа издателя (УЦ) для фильтрации
        -certificate*     Удалить сертификат
        -provname <имя>   Имя провайдера
        -provtype <тип>   Тип провайдера
        -crl              Удалить CRL
        -container <имя>  Имя контейнера закрытого ключа
        -all              Использовать все сертификаты/CRLs
        -silent           Использовать сертификат/CRL в неинтерактивном режиме
        """
        cmd = [f'{self.path} -delete']
        cmd.append(f'-store "{store}"')
        if dn:
            cmd.append(f'-dn {dn}')
        if thumbprint:
            cmd.append(f'-thumbprint {thumbprint}')
        if keyid:
            cmd.append(f'-keyid {keyid}')
        if crl:
            cmd.append('-crl')
        command = ' '.join(cmd)

        self.run_command(command, *args, **kwargs)
        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return True

        return False

    def decode(self, src: str, dest: str, der: bool = True) -> bool:
        """
        ### Декодирование сертификата или crl из der в base64 и наоборот

        Аргументы:
        - src: файл, содержащий сертификат или crl для декодирования
        - dest: файл, в который будет помещен декодированный сертификат или crl
        - der: Если der=True, то декодирует из der в base64, False наоборот

        Возврат:
        - Успешно ли выполнено декодирование
        """

        cmd = [f'{self.path} -decode']
        cmd.append(f'-src "{src}"')
        cmd.append(f'-dest "{dest}"')
        if der:
            cmd.append('-der')
        else:
            cmd.append('-base64')

        command = ' '.join(cmd)

        self.run_command(command)
        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return True

        return False

    def list(self, *args, **kwargs) -> List[Certificate]:
        """
        ### Возвращает список сертификатов
        
        Аргументы:
        - store u<имя>     Имя хранилища сертификатов пользователя (по умолчанию: uMy)
        - store m<имя>     Имя хранилища сертификатов компьютера
            - My – для личных сертификатов
            - Root – для корневых сертификатов Удостоверяющих Центров
            - CA – для сертификатов промежуточных Удостоверяющих Центров
            и списков отозванных сертификатов
            - AddressBook – для сертификатов других пользователей
        - file <путь>      Путь к файлу с сертификатом
        - container <имя>  Имя контейнера закрытого ключа
        - at_signature     Использовать ключ подписи вместо ключа обмена
        - certificate*     Перечислить сертификаты
        - crl              Перечислить CRLs
        - pfx              Отобразить PFX
        - pkcs10           Отобразить файл с PKCS#10
        - dn <CN=..O=..>   DName сертификата для фильтрации
        - thumbprint <хеш> Отпечаток сертификата для фильтрации
        - keyid <id>       ID ключа сертификата для фильтрации
        - authkeyid <id>   ID ключа издателя (УЦ) для фильтрации
        - verbose          Показывать расширенный вывод
        - chain            Отображать цепочку сертификатов
        - stdin            Читать данные из стандартного ввода
        """
        limit = kwargs.pop('limit', None)

        cmd = [f'{self.path} -list']
        command = ' '.join(cmd)

        self.run_command(command, *args, **kwargs)
        self.error = self._check_response_error()

        if self.error == '' and self.response:
            return self._parse(self.response, limit)

        return []

    def get_certs_info(self,
                       file: str,
                       crl: bool = False,
                       chain: bool = False) -> List[Certificate]:
        """
        ### Получает данные о сертификате из файла(в том числе crl)

        Аргументы:
        - file: путь к файлу с сертификатом
        - chain: проверять цепочки сертификатов?
        """
        
        args = ["-verbose"]
        
        if crl:
            args.append("-crl")
            
        if chain:
            args.append("-chain")

        return self.list( *args , file=str(f'"{file}"'))

    def find_ca_for_keyid(self,
                          keyid: str,
                          store: str = "uRoot") -> List[Certificate]:
        """
        ### Поиск сертификатов по ключу УЦ(ID ключа УЦ)

        Аргументы:
        - keyid: идентифкатор ключа УЦ(ID ключа УЦ)
        - store: имя хранилища сертификатов
        """
        return self.list(store=store, keyid=keyid)

    def find_crl_for_keyid(self,
                           keyid: str,
                           store: str = "uRoot") -> List[Certificate]:
        """
        ### Поиск сертификатов crl по ключу УЦ(ID ключа УЦ)

        Аргументы:
        - keyid: идентифкатор ключа УЦ(ID ключа УЦ)
        - store: имя хранилища сертификатов
        """
        return self.list(
            "-crl",
            keyid=keyid,
            store=store,
        )

    def _parse(self, text, limit=None):
        """
        ### Парсит stdout. Возвращает список экземпляров класса Certificate
        """

        text = text.replace("\n" + " " * 22, ', ')
        res = []
        sep_sert = re.compile(r'\d+-{7}')

        for i, item in enumerate(sep_sert.split(text)[1:], start=1):
            cert_data = {}
            for line in item.split('\n'):
                if line == '' or ':' not in line:
                    continue

                if line.startswith('====='):
                    break

                if line.startswith('#0:'):
                    break

                key, val = self._parse_line(line)

                if key in cert_data:

                    cert_data[key] = cert_data[key] + f", {val}"

                else:
                    cert_data[key] = val

            if "номер_crl" in cert_data:
                res.append(self._make_certcrl_object(cert_data))
            else:
                res.append(self._make_cert_object(cert_data))

            if limit and i == limit:
                break

        return res

    @staticmethod
    def _parse_line(line):
        """
        ### Преобразует строку в пару ключ:значение
        """

        key, val = line.split(':', 1)
        key = key.strip().lower().replace(' ', '_')

        val = val.strip()

        # Убираем пробелы, если их более 2 подряд
        pattern = r"\s{2,}"
        val = re.sub(pattern, "", val)

        # Уникальные случаи
        if key == 'серийный_номер':
            val = val.replace('0x', '')

        if key == 'открытый_ключ':
            val = val.replace(', ', '')
            val = re.sub(r"\s+", "", val)

        return key, val

    @staticmethod
    def _make_cert_object(data):
        """
        ### Преобразует словарь с данными сертификата в объект
        """

        def _str_to_datetime(string):
            if string:
                return datetime.strptime(string, '%d/%m/%Y %H:%M:%S UTC')
            return None

        def _check_key(dictionary, key) -> Optional[any]:
            if key in dictionary:
                return dictionary[key]
            return None

        cert_params = {
            "issuer": PersonalInfo(_check_key(data, 'издатель')),
            "subject": PersonalInfo(_check_key(data, 'субъект')),
            "thumbprint": _check_key(data, 'sha1_отпечаток'),
            "serial": _check_key(data, 'серийный_номер'),
            "id_key": _check_key(data, 'идентификатор_ключа'),
            "id_key_ca": _check_key(data, 'id_ключа_уц'),
            "valid_from": _str_to_datetime(_check_key(data, 'выдан')),
            "valid_to": _str_to_datetime(_check_key(data, 'истекает')),
            "chain": _check_key(data, 'цепочка_сертификатов'),
            "data": data
        }

        if 'url_сертификата_уц' in data:
            ca = data['url_сертификата_уц'].split(",")
            cert_params['ca'] = [s.strip() for s in ca]

        if 'url_списка_отзыва' in data:
            crl = data['url_списка_отзыва'].split(",")
            cert_params['crl'] = [s.strip() for s in crl]

        if 'ocsp_url' in data:
            ocsp = data['ocsp_url'].split(",")
            cert_params['ocsp'] = [s.strip() for s in ocsp]

        cert = Certificate(**cert_params)
        return cert

    @staticmethod
    def _make_certcrl_object(data):
        """
        ### Преобразует словарь с данными сертификата CRL в объект
        """

        def _str_to_datetime(string):
            return datetime.strptime(string, '%d/%m/%Y %H:%M:%S UTC')

        certcrl_params = {
            "issuer": PersonalInfo(data['издатель']),
            "valid_from": _str_to_datetime(data['выпущен']),
            "valid_to": _str_to_datetime(data['истекает']),
            "id_key": data['id_ключа_уц'],
            "count": data['записей'],
            "crl_number": data['номер_crl'],
            "data": data
        }

        certcrl = CertificateCRL(**certcrl_params)
        return certcrl